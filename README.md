# Documentation
====================

## Installation Instructions
----------------------------
In RStudio enter the following:

```python
install.packages("devtools")
devtools::install_git(url = "git@10.199.1.xx:R-Packages/nov.color.palette.git", branch = "develop")
```

If you want to use the most stable version substitute "master" for "develop".

## NOV Marketing Explanation
----------------------------
Our **primary color palette** is anchored by the NOV red and gray. 

The **secondary color palette** should only be used for 
* accent colors
* smart art
* chart fills 
* lines. 

These colors should only support the red and gray and never compete with them on the slide.
